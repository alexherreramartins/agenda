package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Agenda {

    private List<Contato> lista;

    public Agenda() {
        this.lista = new ArrayList<>();
    }

    public List<Contato> getLista() {
        return lista;
    }

    public void setLista(List<Contato> lista) {
        this.lista = lista;
    }

    public void iniciarOpcoes() {
        boolean finish = false;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Bem vindo!");

        while (finish != true) {

            IO.imprimeMensagem("");
            IO.imprimeMensagem(".Consulta .Remover .Cadastro .Sair");
            String funcao = scanner.nextLine();

            switch (Enum.valueOf(Funcao.class, funcao)) {

                case Consulta:
                    IO.imprimeContato(consultarContatoEmailNumero());
                    break;

                case Remover:
                    removerContatoEmail();
                    break;

                case Cadastro:
                    iniciarCadastro();
                    break;

                case Sair:
                    finish = true;
                    break;

                default:
                    throw new IllegalStateException("Unexpected value: " + Enum.valueOf(Funcao.class, funcao));
            }

        }
    }

    public void iniciarCadastro() {
        Scanner scanner = new Scanner(System.in);

        IO.imprimeMensagem("Informar o nome:");
        String nome = scanner.nextLine();

        IO.imprimeMensagem("Informar o email(contendo @):");
        String email = scanner.nextLine();

        IO.imprimeMensagem("Informar o numero de contato:");
        String numero = scanner.nextLine();

        adicionarContato(new Contato(nome, email, numero));

    }


    public void adicionarContato(Contato contato) {
        if (contato.getEmail().contains("@")) {
            this.lista.add(contato);
        } else {
            IO.imprimeMensagem("Email inválido! Refazer cadastro.");
        }
    }

    public void removerContatoEmail() {

        Scanner scanner = new Scanner(System.in);
        IO.imprimeMensagem("Informar o email a ser removido:");
        String email = scanner.nextLine();

        Predicate<Contato> byEmail = contato -> contato.getEmail().equals(email);

        if (email.contains("@")) {
            this.lista.removeIf(byEmail);
        } else {
            IO.imprimeMensagem("Email inválido! Tente novamente.");
        }

    }

    public Contato consultarContatoEmailNumero() {

        Scanner scanner = new Scanner(System.in);
        IO.imprimeMensagem("Informar o email ou numero a ser consultado:");
        String valor = scanner.nextLine();

        Predicate<Contato> byValor;
        if (valor.contains("@")) {
            byValor = contato -> contato.getEmail().equals(valor);
        } else {
            byValor = contato -> contato.getNumero().equals(valor);
        }

        return lista.stream().filter(byValor).findAny().orElse(null);

    }
}
