package br.com.itau;

public class IO {

    public static void imprimeMensagem(String valor){
        System.out.println(valor);
    }

    public static void imprimeContato(Contato contato) {

        if (contato != null) {
            imprimeMensagem("Nome: " + contato.getNome());
            imprimeMensagem("e-mail: " + contato.getEmail());
            imprimeMensagem("numero: " + contato.getNumero());
        } else {
            imprimeMensagem("Contato nao pertence a lista!");
        }
    }
}
